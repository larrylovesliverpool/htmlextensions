﻿using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;

namespace HTMLExtensions.ListGroup
{
    public static class BootstrapListGroup
    {
        public static IHtmlString BootStrapListGroup(this HtmlHelper helper,  string[] content , string identifier = "ListGroup")
        {
            StringBuilder html = new StringBuilder();

            html.Append(@"<ul id=""").Append(identifier).Append(@"""").Append(@"class=""list-group"">");

            foreach(var item in content)
            {
                html.Append(@"<li class=""list-group-item"">").Append(item).Append(@"</li>");
            }

            html.Append(@"</ul>");

            return new HtmlString(html.ToString());
        }

        // ***************************************
        // display 'options' are Active and Flush.
        // ***************************************

        public static IHtmlString BootStrapListGroup(this HtmlHelper helper, string[] content,
                                                     string identifier = "ListGroup", string options = "" )
        {
            StringBuilder html = new StringBuilder();
            bool active = false;

            html.Append(@"<ul id=""").Append(identifier).Append(@""" ").Append(@"class=""list-group"">");
            if (options.Contains("Flush")) html.Replace("list-group", "list-group list-group-flush");

            foreach (var item in content)
            {
                if (!active && options.Contains("Active"))
                {
                    html.Append(@"<li class=""list-group-item active"">").Append(item).Append(@"</li>");
                    active = true;
                }
                else
                {
                    html.Append(@"<li class=""list-group-item"">").Append(item).Append(@"</li>");
                }
            }

            html.Append(@"</ul>");

            return new HtmlString(html.ToString());
        }

        public static IHtmlString BootStrapListGroup(this HtmlHelper helper, List<string> content)
        {
            StringBuilder html = new StringBuilder();

            html.Append(@"<ul class=""list-group"">");
            content.ForEach(item => html.Append(listLineBuild(item)));
            html.Append(@"</ul>");

            return new HtmlString(html.ToString());
        }

        private static string listLineBuild(string item)
        {
            StringBuilder htmlLine = new StringBuilder();

            htmlLine.Append(@"<li class=""list-group-item"">").Append(item).Append(@"</li>");

            return htmlLine.ToString();
        }
    }
}
