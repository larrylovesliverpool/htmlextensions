﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTMLExtensions.BootStrapListGroup
{
    public class ListGroupItems
    {
        private string _content;
        private string _href;

        public string Content
        {
            get
            {
                return _content;
            }

            set
            {
                _content = value;
            }
        }

        public string Href
        {
            get
            {
                return _href;
            }

            set
            {
                _href = value;
            }
        }
    }
}
