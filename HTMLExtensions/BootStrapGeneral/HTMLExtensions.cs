﻿using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HTMLExtensions.General
{
    public static class HTMLExtensions
    {
        public static IHtmlString BootStrapBadge(this HtmlHelper helper, string content, string context = "Default" )
        {
            StringBuilder html = new StringBuilder();

            try
            {
                html.Append(@"<span class=""badge badge-").Append(context.ToLower()).Append(@""" >").Append(content).Append(@"</span>");
            }
            catch
            {
                html.Clear().Append("Unable to create badge.");
            }

            return new HtmlString(html.ToString());
        }

        public static IHtmlString BootStrapAlert(this HtmlHelper helper, string content, string context = "Default")
        {
            StringBuilder html = new StringBuilder();

            try
            {
                html.Append(@"<div class=""alert alert-").Append(context.ToLower()).Append(@""" >").Append(content).Append(@"</div>");
            }
            catch
            {
                html.Clear().Append("Unable to create badge.");
            }

            return new HtmlString(html.ToString());
        }

    }
}
