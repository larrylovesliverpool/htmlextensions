﻿using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HTMLExtensions.BootStrapBreadCrumb
{
    public static class HTMLExtensionsBootStrapBreadCrumb
    {

        public static IHtmlString BootStrapBreadCrumb(this HtmlHelper helper, List<BreadCrumbItem> items, string identifier = "BreadCrumb")
        {
            StringBuilder html = new StringBuilder();

            try
            {
                html.Append(@"<nav aria-label=""breadcrumb"" id=""").Append(identifier).Append(@""" ").Append(@""">");
                html.Append(@"<ol class=""breadcrumb"" >");
               
                items.ForEach(item => html.Append(crumbLineBuildBootStrap(item)));

                html.Append(@"</ol>");
                html.Append(@"</nav>");
            }
            catch
            {
                html.Clear().Append("There was a problem building the breadcrumb.");
            }

            return new HtmlString(html.ToString());
        }

        public static IHtmlString BreadCrumb(this HtmlHelper helper, List<BreadCrumbItem> items, string identifier = "BreadCrumb")
        {
            StringBuilder html = new StringBuilder();

            try
            {
                html.Append(@"<ul id=""").Append(identifier).Append(@""" ").Append(@"class=""breadcrumbtrail"">");
                items.ForEach(item => html.Append(crumbLineBuild(item)));
                html.Append(@"</ul>");
            }
            catch
            {
                html.Clear().Append("There was a problem building the breadcrumb.");
            }

            return new HtmlString(html.ToString());
        }

        private static string crumbLineBuildBootStrap(BreadCrumbItem item)
        {
            StringBuilder htmlLine = new StringBuilder();

            htmlLine.Append(@"<li class=""breadcrumb-item""><a href=""").Append(item.Href).Append(@""" >").Append(item.Content).Append(@" </a></li>");

            return htmlLine.ToString();
        }

        private static string crumbLineBuild(BreadCrumbItem item)
        {
            StringBuilder htmlLine = new StringBuilder();

            htmlLine.Append(@"<li><a href=""").Append(item.Href).Append(@""" >").Append(item.Content).Append(@" </a></li>");

            return htmlLine.ToString();
        }
    }
}
