﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTMLExtensions.BootStrapBreadCrumb
{
    public class BreadCrumbItem
    {
        private string _content;
        private string _href;

        public string Content
        {
            get
            {
                return _content;
            }

            set
            {
                _content = value;
            }
        }

        public string Href
        {
            get
            {
                return _href;
            }

            set
            {
                _href = value;
            }
        }

        public BreadCrumbItem() { }

        public BreadCrumbItem(string content, string href)
        {
            this.Content = content;
            this.Href = href;
        }
    }
}
